#!/usr/bin/python

import numpy as np
import scipy.integrate as scint
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import seaborn as sns
sns.set_style("darkgrid")

from plotdf import plotdf

import argparse
from random import random

# The SIR model differential equations.
def deriv(y, t, N, beta, gamma):
    S, I, R = y
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    return dSdt, dIdt, dRdt

def deriv_adim(y, t, N, mu):
    S, I, R = y
    dSdt = -S * I / N
    dIdt = (S / N - mu) * I 
    dRdt = mu * I 
    return dSdt, dIdt, dRdt

def solveSIRdet(y0, tau, N, mu):
  # Integrate the SIR equations over the time grid, t
  ret = scint.odeint(deriv_adim, y0, tau, args=(N, mu))
  s, i, r = ret.T

  return {"S":s, "I":i, "R":r}

def solveSIRstoch(y0, tau, N, mu, beta, gamma):
  s0, i0, r0 = y0  #suscettibili iniziali
  i=[i0*N]; r=[r0*N]; s=[s0*N]

  for _t in range(len(tau)):
    print("_t:",_t)
    infected=0 #inizializzo la variabile ogni dt per tener traccia dei contagiati ogni ciclo
    _s = int(s[_t]); _i = int(i[_t]);
    pInfection = _s*_i*beta/(N*(N-1)/2)
    print("\tProbability of getting infected this round:",pInfection)
    print("\tProbability of recovering:",mu)
    print("\tSimulating transition S->I for %d agents:"%_s)
    for _ in range(_s):
      p=random()
      if p<=pInfection:
        infected+=1
   
    recovered=0 #inizializzo la variabile ogni dt per tener traccia dei recovered ogni ciclo
    print("\tSimulating transition I->R for %d agents:"%_i)    
    for _ in range(_i):
      p=random()      
      if p<=gamma:
        recovered+=1
    print("\t New infected: %d  New recovered: %d"%(infected, recovered))  
    s.append(s[_t]-infected)
    i.append(i[_t]+infected-recovered)
    r.append(r[_t]+recovered)
  
  # Normalization
  s = np.asarray(s)/N
  i = np.asarray(i)/N
  r = np.asarray(r)/N

  return {"S":s, "I":i, "R":r}

def plotPhase(tau, xbound, ybound, gridsteps, tmax, nsteps, N, mu):
  parameters = {"N":N,"mu":mu}
  fig = plt.figure(figsize=(8,8), facecolor='w')
  axes = fig.add_subplot(111, axisbelow=True)

  x = np.linspace(xbound[0],xbound[1],gridsteps)
  y = np.linspace(ybound[0],ybound[1],gridsteps)
  xx,yy = np.meshgrid(x,y)
  uu = np.empty_like(xx)
  vv = np.empty_like(yy)
  for i in range(gridsteps):
    for j in range(gridsteps):
      res = deriv_adim(np.array([xx[i,j],yy[i,j], 0.0]), tau, **parameters)
      uu[i,j] = res[0]
      vv[i,j] = res[1]

  artists = []
  EE = np.hypot(uu, vv)

  #I = plt.imshow(EE,extent=[np.min(xx),np.max(xx),np.min(yy),np.max(yy)],cmap='coolwarm')
  axes.quiver(
      xx,yy,uu,vv,EE,
      cmap="autumn", 
      #norm=colors.LogNorm(vmin=M.min(),vmax=M.max()),
      width=0.002)

  plt.title(r"2D Phase space for SIR model")
  axes.grid(b=True, which='major', c='w', lw=2, ls='-')
  axes.axvline(x=mu*N, color="k")
  axes.set_xlabel(r'S')
  axes.set_ylabel(r'I')
  plt.xlim(xbound)
  plt.ylim(ybound)
  plt.savefig("ps.png")

  return axes

def plotTrajectories(inits, axes, tmax, nsteps, tdir, N, mu):
  parameters = {"N":N,"mu":mu}
  # Plot some trajectories over the phase space in axes
  def g(x,t):
    return np.array(deriv_adim(x, t, **parameters))

  def bg(x,t):
    return -1.0*np.array(deriv_adim(x, t, **parameters))

  t = np.linspace(0, int(tmax), int(nsteps))
  for y0 in inits:
    traj_f = np.empty((0,2))
    traj_b = np.empty((0,2))
    if tdir in ["forward","both"]:
      traj_f = scint.odeint(g,y0,t)
    if tdir in ["backward","both"]:
      traj_b = scint.odeint(bg,y0,t)
    
    if tdir != "both":
      traj = traj_f
    else:
      traj = np.vstack((np.flipud(traj_b),traj_f))
    axes.plot(traj[:,0],traj[:,1])
  
  plt.savefig("ps_traj.png")

def plotAlls(tau, sd, ss):

  # Plot the data on three separate curves for S(t), I(t) and R(t)
  fig = plt.figure(facecolor='w')
  ax = fig.add_subplot(111, axisbelow=True)

  ax.plot(tau, sd["S"], 'b', alpha=0.5, lw=2, label='Susceptible')
  ax.plot(tau, sd["I"], 'r', alpha=0.5, lw=2, label='Infected')
  ax.plot(tau, sd["R"], 'g', alpha=0.5, lw=2, label='Recovered')
  plt.title(r"Adimensional SIR model")
  ax.set_xlabel(r'$\tau$')
  ax.set_ylabel(r'Ratio \(\%\)')
  #ax.set_ylim(0,1.2)
  ax.yaxis.set_tick_params(length=0)
  ax.xaxis.set_tick_params(length=0)
  ax.grid(b=True, which='major', c='w', lw=2, ls='-')
  legend = ax.legend()
  legend.get_frame().set_alpha(0.5)
  for spine in ('top', 'right', 'bottom', 'left'):
    ax.spines[spine].set_visible(False)
  plt.savefig("sir.png")

  ax.plot(tau, ss["S"][:-1], 'b--', alpha=0.5, lw=2, label='Susceptible Stoch')
  ax.plot(tau, ss["I"][:-1], 'r--', alpha=0.5, lw=2, label='Infected Stoch')
  ax.plot(tau, ss["R"][:-1], 'g--', alpha=0.5, lw=2, label='Recovered Stoch')

  legend = ax.legend()
  legend.get_frame().set_alpha(0.5)
  for spine in ('top', 'right', 'bottom', 'left'):
    ax.spines[spine].set_visible(False)
  plt.savefig("sir_alls.png")


def main(args):
  plt.rc('text', usetex=True)

  # Total population, N.
  N = args.agents
  # Initial number of infected and recovered individuals, I0 and R0.
  I0, R0 = args.i0, args.r0
  # Everyone else, S0, is susceptible to infection initially.
  S0 = N - I0 - R0
  # Contact rate, beta, and mean recovery rate, gamma, (in 1/days).
  beta, gamma = args.beta, args.gamma 
  # A grid of time points (in days)
  t = np.linspace(0, args.days, args.days)

  tau = t*beta*N
  mu = gamma/(beta*N)
  print("Adimentional virulence mu:",mu)

  # Initial conditions vector
  y0 = 1.0*S0/N, 1.0*I0/N, 1.0*R0/N
  sir_det = solveSIRdet(y0, tau, N, mu)
  sir_stoch = solveSIRstoch(y0, tau, N, mu, beta, gamma)

  xbound, ybound = (0.0, 1.0), (0.0, 1.0)
  gridsteps = 30; tmax=2e3; nsteps=1e2; tdir="forward"

  axes = plotPhase(tau, xbound, ybound, gridsteps, tmax, nsteps, N, mu)

  inits=[(0.9, 0.7, 0.0),(0.9, 0.6, 0.0),
         (0.9, 0.4, 0.0), (0.9, 0.2, 0.0)]  
  plotTrajectories(inits, axes, tmax*4, nsteps*4, tdir, N, mu)

  plotAlls(tau, sir_det, sir_stoch)

if __name__ == "__main__":  
  parser = argparse.ArgumentParser()
  parser.add_argument('-d','--days', type=int, default=40, help="Timesteps to run the model for" )          
  parser.add_argument('-n','--agents', type=int, default=1000, help="Initial population" )  
  parser.add_argument('--i0', type=int, default=1, help="Initial infected" ) 
  parser.add_argument('--r0', type=int, default=0, help="Initial recovered" )     
  parser.add_argument('--beta', type=float, default=0.9, help="Contact rate" )
  parser.add_argument('--gamma', type=float, default=0.2, help="Mean recovery rate" )    
  parser.set_defaults(func=main)  
  
  args = parser.parse_args()  
  args.func(args)  
