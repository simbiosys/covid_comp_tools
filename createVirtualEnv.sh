#!/bin/bash

# Install c++ dependencies
sudo dnf install spatialindex-devel geos-devel proj-devel

mkvirtualenv mesa -p $(which python3)
pip install -r requirements.txt
