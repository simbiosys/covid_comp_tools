import os, sys
import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(message)s', 
          datefmt='%Y/%m/%d %I:%M:%S %p', 
          level=logging.INFO)


_pckg_basedir = os.path.dirname(os.path.abspath(__file__))
_sandbox_defs = {
  'environments':os.path.join(_pckg_basedir,"env_def"),
  'templates':os.path.join(_pckg_basedir,"templates/xmlTemplates"),
  'models':os.path.join(_pckg_basedir,"models/")  
}

from .behaviours.behaviourFactory import BehaviourFactory
from .behaviours.actions import Action
_behaviourFactory = BehaviourFactory()
_behaviourFactory.load_all()

from .brainModelFactory import BrainModelFactory
_brainModelFactory = BrainModelFactory()
_brainModelFactory.load_all()
_brainModelFactory.list_all()

from .templates.templateFactory import XAgentFactory
_agentTemplateFactory = XAgentFactory()
_agentTemplateFactory.load()

from .environments import EnvironmentFactory
_environmentFactory = EnvironmentFactory()
_environmentFactory.load()        

from .xworlds import XAgent, MultiEnvironmentWorld, PopulationRequest, Event, RewardRule


